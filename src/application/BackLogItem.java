package application;

import java.util.StringTokenizer;

import org.trello4j.model.Card;

public class BackLogItem {
	private String Name,Description,Comments;
	private int StoryPoints,OS_Prob,FS_Prob,LC_Prob;
	
	public enum Category{Audio_Video,
		Business_Enterprise,
		Communications,
		Development,
		Home_Education,
		Games,
		Graphics,
		Science_Engineering,
		Security_Utilities,
		System_Administration
					};
	private Category cat;
	
	
	
	public Category getCat() {
		return cat;
	}
	public void setCat(Category cat) {
		this.cat = cat;
	}
	public int getOS_Prob() {
		return OS_Prob;
	}
	public void setOS_Prob(int oS_Prob) {
		OS_Prob = oS_Prob;
	}
	public int getFS_Prob() {
		return FS_Prob;
	}
	public void setFS_Prob(int fS_Prob) {
		FS_Prob = fS_Prob;
	}
	public int getLC_Prob() {
		return LC_Prob;
	}
	public void setLC_Prob(int lC_Prob) {
		LC_Prob = lC_Prob;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public BackLogItem(String name, String description, String comments, int storyPoints,int OS_Prob,int FS_Prob,int LC_Prob) {
		super();
		Name = name;
		Description = description;
		Comments = comments;
		StoryPoints = storyPoints;
		this.OS_Prob=OS_Prob;
		this.FS_Prob=FS_Prob;
		this.LC_Prob=LC_Prob;
	}
	
	public BackLogItem(Card card)
	{
		Name=card.getName();
		StringTokenizer tok=new StringTokenizer(card.getDesc(),"#");
		StoryPoints=Integer.valueOf(tok.nextToken());
		if(tok.hasMoreTokens())
		Description=tok.nextToken();
		else
		{
			Description="";
		}
		System.out.println(StoryPoints+" "+Description);
		Comments="";
	}
	public int getStoryPoints() {
		return StoryPoints;
	}
	public void setStoryPoints(int storyPoints) {
		StoryPoints = storyPoints;
	}
	

}
