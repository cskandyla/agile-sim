package application;

import java.util.ArrayList;
import java.util.HashMap;

import application.Component.Choice;

public class Simulation {
	ArrayList<BackLogItem> BackLog;
	ArrayList<Component> Components;
	int num_runs,sprint_duration,story_points_per_sprint;
	double avg_story_points;
	HashMap<BackLogItem.Category,CostMultiplierPair> multipliers;
	
	
	
	
	public  Simulation(ArrayList<BackLogItem> BackLog,int num_runs,int sprint_duration,int story_points_per_sprint,HashMap<BackLogItem.Category,CostMultiplierPair> multipliers)
	{
		
		this.BackLog=BackLog;
		this.num_runs=num_runs;
		this.sprint_duration=sprint_duration;
		this.story_points_per_sprint=story_points_per_sprint;
		this.multipliers=multipliers;
		
		//InitMultipliers(Costs);
		Components=new ArrayList<Component>();
		for(int i=0;i<BackLog.size();i++)
		{	
			if(multipliers.containsKey(BackLog.get(i).getCat()))
			{
				CostMultiplierPair costmult=multipliers.get(BackLog.get(i).getCat());
				double osmult=costmult.getOS_Mult();
				double lcmult=costmult.getLC_Mult();
				Components.add(new Component(BackLog.get(i).getName(), BackLog.get(i).getOS_Prob(),BackLog.get(i).getFS_Prob(),
						
						BackLog.get(i).getLC_Prob(),osmult,1,lcmult, BackLog.get(i).getStoryPoints()));	
				
			}
			
			else
			{
				Components.add(new Component(BackLog.get(i).getName(), BackLog.get(i).getOS_Prob(),BackLog.get(i).getFS_Prob(),
						BackLog.get(i).getLC_Prob(), 0.7, 1, 0.4, BackLog.get(i).getStoryPoints()));	
				
			}
		}
	}
	
	public HashMap<BackLogItem.Category, CostMultiplierPair> getMultipliers() {
		return multipliers;
	}

	public void setMultipliers(HashMap<BackLogItem.Category, CostMultiplierPair> multipliers) {
		this.multipliers = multipliers;
	}

	public void AddCostCategory(BackLogItem.Category cat,CostMultiplierPair cost)
	{
		multipliers.put(cat, cost);
	}
	
	
	public void Run()
	{
		
		double story_points=0;
		for(int i=0;i<num_runs;i++)
		{	
			
			for(int j=0;j<Components.size();j++)
			{
				Choice choice= Components.get(j).MakeChoice();
				story_points+=Components.get(j).Update(choice);
				//
					
			}
			
			
		}
		
		avg_story_points=story_points/num_runs;
		
	}
	
	public ArrayList<Component> getComponents()
	{
		return this.Components;
	}
	
	public double Estimated_Story_Points()
	{
		return  avg_story_points;
	}
	public int Estimated_Sprints()
	{
		return (int) Math.ceil(avg_story_points/story_points_per_sprint);
	}
	
	public int Estimated_Duration()
	{
		return (int) Math.ceil(Estimated_Sprints()*sprint_duration);
	}


	public double Velocity() {
		return avg_story_points/story_points_per_sprint;
		
	}
	
	
	
	

}
