package application;

public class CostMultiplierPair {
	double OS_Mult,LC_Mult;

	public CostMultiplierPair(double oS_Mult, double lC_Mult) {
		super();
		OS_Mult = oS_Mult;
		LC_Mult = lC_Mult;
	}

	public double getOS_Mult() {
		return OS_Mult;
	}

	public void setOS_Mult(double oS_Mult) {
		OS_Mult = oS_Mult;
	}

	public double getLC_Mult() {
		return LC_Mult;
	}

	public void setLC_Mult(double lC_Mult) {
		LC_Mult = lC_Mult;
	}

}
