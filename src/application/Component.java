package application;
import java.util.Random;

import application.Component.Choice;

public class Component {
String name;
int Story_Points;
int OS_Prob,FS_Prob,LC_Prob;
int num_OS,num_FS,num_LC;
double avg_story_points,sum_story_points;
double OS_Mult,FS_Mult,LC_Mult;
public enum Choice{OS,FS,LC};

Choice choice;


public Component(String name, int oS_Prob, int fS_Prob, int lC_Prob, double oS_Mult, double fS_Mult, double lC_Mult,
		int  story_points) {
	super();
	this.name = name;
	OS_Prob = oS_Prob;
	FS_Prob = fS_Prob;
	LC_Prob = lC_Prob;
	OS_Mult = oS_Mult;
	FS_Mult = fS_Mult;
	LC_Mult = lC_Mult;
	int num_OS=0;
	int num_FS=0;
	int num_LC=0;
	avg_story_points=0;
	sum_story_points=0;
	this.choice = null;
	this.Story_Points=story_points;
	
}

public double getAvgStoryPoints()
{
	return this.avg_story_points;
}

public int getNum_OS() {
	return num_OS;
}


public int getNum_FS() {
	return num_FS;
}


public int getNum_LC() {
	return num_LC;
}


public int getStory_Points() {
	return Story_Points;
}


public void setStory_Points(int story_Points) {
	Story_Points = story_Points;
}


public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getOS_Prob() {
	return OS_Prob;
}
public void setOS_Prob(int oS_Prob) {
	OS_Prob = oS_Prob;
}
public int getFS_Prob() {
	return FS_Prob;
}
public void setFS_Prob(int fS_Prob) {
	FS_Prob = fS_Prob;
}
public int getLC_Prob() {
	return LC_Prob;
}
public void setLC_Prob(int lC_Prob) {
	LC_Prob = lC_Prob;
}
public double getOS_Mult() {
	return OS_Mult;
}
public void setOS_Mult(double oS_Mult) {
	OS_Mult = oS_Mult;
}
public double getFS_Mult() {
	return FS_Mult;
}
public void setFS_Mult(double fS_Mult) {
	FS_Mult = fS_Mult;
}
public double getLC_Mult() {
	return LC_Mult;
}
public void setLC_Mult(double lC_Mult) {
	LC_Mult = lC_Mult;
}
public Choice getChoice() {
	return choice;
}
public void setChoice(Choice choice) {
	this.choice = choice;
}
public Choice MakeChoice() {
	 Random randomGenerator = new Random();
	int randint=randomGenerator.nextInt(100);
	//OS is 0-OS_Prob FS is OS_Prob-OS_Prob+FS_Prob 
	
	if(randint<this.OS_Prob)
	{	
		return Choice.OS;
	}
	else if(randint<this.OS_Prob+this.FS_Prob)
	{
		return Choice.FS;
	}
	else
	{
		return Choice.LC;
	}
	
	// TODO Auto-generated method stub

}

public double Update(Choice c)
{
	
	switch(c)
	{
	case OS:
		num_OS++;
		sum_story_points+=(Story_Points*OS_Mult);
		avg_story_points=sum_story_points/(num_OS+num_FS+num_LC);
		return OS_Mult*Story_Points;
	case FS:
		num_FS++;
		sum_story_points+=(Story_Points*FS_Mult);
		avg_story_points=sum_story_points/(num_OS+num_FS+num_LC);
		return FS_Mult*Story_Points;
	case LC:
		num_LC++;
		sum_story_points+=(Story_Points*LC_Mult);
		avg_story_points=sum_story_points/(num_OS+num_FS+num_LC);
		return LC_Mult*Story_Points;
	}
	return -1;
	
	
}

public Choice Dominant_Choice()
{
	int max=Math.max(num_OS, num_FS);
	max=Math.max(max, num_LC);
	
	if(max==num_OS)
	{
		return Choice.OS;
	}
	else if(max==num_FS)
	{
		return Choice.FS;
	}
	else 
	{
		return Choice.LC;
	}
	
}

	

}
