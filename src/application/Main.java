package application;
	
import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;

import org.trello4j.Trello;
import org.trello4j.TrelloImpl;
import org.trello4j.model.Board;
import org.trello4j.model.Card;

import com.fxgraph.graph.ButtonCell;
import com.fxgraph.graph.CellType;
import com.fxgraph.graph.Graph;
import com.fxgraph.graph.LabelCell;
import com.fxgraph.graph.Layout;
import com.fxgraph.graph.Model;
import com.fxgraph.graph.RandomLayout;
import com.fxgraph.graph.TreeLayout;

import application.BackLogItem.Category;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.*;



public class Main extends Application  {
	Scene Login_scene;
	Scene List_Select_Scene;
	Scene Backlog_Scene;
	Scene Component_Graph_Scene;
	Scene Simulation_Scene;
	Scene Software_Category_Weights_Scene;
	//TreeView to hold BackLogEntries;
	TreeView<String> BackLogEntryTree;
	Graph graph;
	Trello trello;
	String username="skandylascharilaos";
	Board current_board=null;
	HashMap<String,BackLogItem> Backlog;
	
	Label num_story_points_lbl;
	Label estimated_velocity_lbl;
	Label estimated_num_days_lbl;
	Label estimated_sprints_lbl;
	AreaChart<Number,Number> BurndownChart;
	HashMap<BackLogItem.Category,CostMultiplierPair> multipliers;
	
	
	public void Create_Software_Category_Weights_Scene(Stage window)
	{
		GridPane Software_Category_Weights_Grid=new GridPane();
		Software_Category_Weights_Grid.setPadding(new Insets(10,10,10,10));
		Software_Category_Weights_Grid.setVgap(10);
		Software_Category_Weights_Grid.setHgap(10);
		//usename stuff
		Label Weights_lbl=new Label("Open Souce and From Scratch Cost:");
		GridPane.setConstraints(Weights_lbl, 0, 0);
		
		Label audio_video_lbl=new Label("Audio-Video:");
		GridPane.setConstraints(audio_video_lbl, 0, 1);
		TextField audio_video_os_txt=new TextField();
		audio_video_os_txt.setText("0.7");
		GridPane.setConstraints(audio_video_os_txt, 1, 1);
		TextField audio_video_lc_txt=new TextField();
		audio_video_lc_txt.setText("0.4");
		GridPane.setConstraints(audio_video_lc_txt, 2, 1);
		
		Label Business_Enterprise_lbl=new Label("Business-Enterprise:");
		GridPane.setConstraints(Business_Enterprise_lbl, 0, 2);
		TextField Business_Enterprise_os_txt=new TextField();
		Business_Enterprise_os_txt.setText("0.7");
		GridPane.setConstraints(Business_Enterprise_os_txt, 1, 2);
		TextField Business_Enterprise_lc_txt=new TextField();
		Business_Enterprise_lc_txt.setText("0.4");
		GridPane.setConstraints(Business_Enterprise_lc_txt, 2, 2);
		
		Label Communications_lbl=new Label("Communications:");
		GridPane.setConstraints(Communications_lbl, 0, 3);
		TextField Communications_os_txt=new TextField();
		Communications_os_txt.setText("0.7");
		GridPane.setConstraints(Communications_os_txt, 1, 3);
		TextField Communications_lc_txt=new TextField();
		Communications_lc_txt.setText("0.4");
		GridPane.setConstraints(Communications_lc_txt, 2, 3);
		
		Label Development_lbl=new Label("Development:");
		GridPane.setConstraints(Development_lbl, 0, 4);
		TextField Development_os_txt=new TextField();
		Development_os_txt.setText("0.7");
		GridPane.setConstraints(Development_os_txt, 1, 4);
		TextField Development_lc_txt=new TextField();
		Development_lc_txt.setText("0.4");
		GridPane.setConstraints(Development_lc_txt, 2, 4);
		
		Label Home_Education_lbl=new Label("Home-Education:");
		GridPane.setConstraints(Home_Education_lbl, 0, 5);
		TextField Home_Education_os_txt=new TextField();
		Home_Education_os_txt.setText("0.7");
		GridPane.setConstraints(Home_Education_os_txt, 1, 5);
		TextField Home_Education_lc_txt=new TextField();
		Home_Education_lc_txt.setText("0.4");
		GridPane.setConstraints(Home_Education_lc_txt, 2, 5);
		
		Label Games_lbl=new Label("Games:");
		GridPane.setConstraints(Games_lbl, 0, 6);
		TextField Games_os_txt=new TextField();
		Games_os_txt.setText("0.7");
		GridPane.setConstraints(Games_os_txt, 1, 6);
		TextField Games_lc_txt=new TextField();
		Games_lc_txt.setText("0.4");
		GridPane.setConstraints(Games_lc_txt, 2, 6);
		
		Label Graphics_lbl=new Label("Graphics:");
		GridPane.setConstraints(Graphics_lbl, 0, 7);
		TextField Graphics_os_txt=new TextField();
		Graphics_os_txt.setText("0.7");
		GridPane.setConstraints(Graphics_os_txt, 1, 7);
		TextField Graphics_lc_txt=new TextField();
		 Graphics_lc_txt.setText("0.4");
		GridPane.setConstraints( Graphics_lc_txt, 2, 7);
		
		Label Science_Engineering_lbl=new Label("Science-Engineering:");
		GridPane.setConstraints(Science_Engineering_lbl, 0, 8);
		TextField Science_Engineering_os_txt=new TextField();
		Science_Engineering_os_txt.setText("0.7");
		GridPane.setConstraints(Science_Engineering_os_txt, 1, 8);
		TextField Science_Engineering_lc_txt=new TextField();
		Science_Engineering_lc_txt.setText("0.4");
		GridPane.setConstraints(Science_Engineering_lc_txt, 2, 8);
		
		
		
		Label Security_Utilities_lbl=new Label("Security-Utilities:");
		GridPane.setConstraints(Security_Utilities_lbl, 0, 9);
		TextField Security_Utilities_os_txt=new TextField();
		Security_Utilities_os_txt.setText("0.7");
		GridPane.setConstraints(Security_Utilities_os_txt, 1, 9);
		TextField Security_Utilities_lc_txt=new TextField();
		Security_Utilities_lc_txt.setText("0.4");
		GridPane.setConstraints(Security_Utilities_lc_txt, 2, 9);
		
		Label System_Administration_lbl=new Label("System-Administration:");
		GridPane.setConstraints(System_Administration_lbl, 0, 10);
		TextField System_Administration_os_txt=new TextField();
		System_Administration_os_txt.setText("0.7");
		GridPane.setConstraints(System_Administration_os_txt, 1, 10);
		TextField System_Administration_lc_txt=new TextField();
		System_Administration_lc_txt.setText("0.4");
		GridPane.setConstraints(System_Administration_lc_txt, 2, 10);
		
		
		Button SaveButton=new Button("Save");
		GridPane.setConstraints(SaveButton, 0, 11);
		SaveButton.setOnAction(e->UpdateWeights(audio_video_os_txt,audio_video_lc_txt,
				Business_Enterprise_os_txt,Business_Enterprise_lc_txt,
				Communications_os_txt,Communications_lc_txt,
				Development_os_txt,Development_lc_txt,
				Home_Education_os_txt,Home_Education_lc_txt,
				Games_os_txt,Games_lc_txt,
				Graphics_os_txt,Graphics_lc_txt,
				Science_Engineering_os_txt,Science_Engineering_lc_txt,
				Security_Utilities_os_txt,Security_Utilities_lc_txt,
				System_Administration_os_txt,System_Administration_lc_txt, window));
		

		
		Software_Category_Weights_Grid.getChildren().addAll(
				Weights_lbl,
				audio_video_lbl,audio_video_os_txt,audio_video_lc_txt,
				Business_Enterprise_lbl,Business_Enterprise_os_txt,Business_Enterprise_lc_txt,
				Communications_lbl,Communications_os_txt,Communications_lc_txt,
				Development_lbl,Development_os_txt,Development_lc_txt,
				Home_Education_lbl,Home_Education_os_txt,Home_Education_lc_txt,
				Games_lbl,Games_os_txt,Games_lc_txt,
				Graphics_lbl,Graphics_os_txt,Graphics_lc_txt,
				Science_Engineering_lbl,Science_Engineering_os_txt,Science_Engineering_lc_txt,
				Security_Utilities_lbl,Security_Utilities_os_txt,Security_Utilities_lc_txt,
				System_Administration_lbl,System_Administration_os_txt,System_Administration_lc_txt,
				SaveButton
				);
				
				
				
				
		Software_Category_Weights_Scene=new Scene(Software_Category_Weights_Grid,625,800);
		
	}
	
	
	
	
	private void UpdateWeights(TextField audio_video_os_txt, TextField audio_video_lc_txt,
			 TextField business_Enterprise_os_txt, TextField business_Enterprise_lc_txt,
			 TextField communications_os_txt, TextField communications_lc_txt,
			 TextField development_os_txt, TextField development_lc_txt, 
			 TextField home_Education_os_txt, TextField home_Education_lc_txt,
			 TextField games_os_txt,TextField games_lc_txt,
			 TextField graphics_os_txt, TextField graphics_lc_txt,
			 TextField science_Engineering_os_txt, TextField science_Engineering_lc_txt,
			 TextField security_Utilities_os_txt, TextField security_Utilities_lc_txt,
			 TextField system_Administration_os_txt,TextField system_Administration_lc_txt,
			 Stage window) {
		
		multipliers.put(Category.Audio_Video, new CostMultiplierPair(Double.parseDouble(audio_video_os_txt.getText()),Double.parseDouble(audio_video_lc_txt.getText())));
		multipliers.put(Category.Business_Enterprise, new CostMultiplierPair(Double.parseDouble(business_Enterprise_os_txt.getText()),Double.parseDouble(business_Enterprise_lc_txt.getText())));
		multipliers.put(Category.Communications, new CostMultiplierPair(Double.parseDouble(communications_os_txt.getText()),Double.parseDouble(communications_lc_txt.getText())));
		multipliers.put(Category.Development, new CostMultiplierPair(Double.parseDouble(development_os_txt.getText()),Double.parseDouble(development_lc_txt.getText())));
		multipliers.put(Category.Home_Education, new CostMultiplierPair(Double.parseDouble(home_Education_os_txt.getText()),Double.parseDouble(home_Education_lc_txt.getText())));
		multipliers.put(Category.Games, new CostMultiplierPair(Double.parseDouble(games_os_txt.getText()),Double.parseDouble(games_lc_txt.getText())));
		multipliers.put(Category.Graphics, new CostMultiplierPair(Double.parseDouble(graphics_os_txt.getText()),Double.parseDouble(graphics_lc_txt.getText())));
		multipliers.put(Category.Science_Engineering, new CostMultiplierPair(Double.parseDouble(science_Engineering_os_txt.getText()),Double.parseDouble(science_Engineering_lc_txt.getText())));
		multipliers.put(Category.Security_Utilities, new CostMultiplierPair(Double.parseDouble(security_Utilities_os_txt.getText()),Double.parseDouble(security_Utilities_lc_txt.getText())));
		multipliers.put(Category.System_Administration, new CostMultiplierPair(Double.parseDouble(system_Administration_os_txt.getText()),Double.parseDouble(system_Administration_lc_txt.getText())));
		
		window.setScene(Backlog_Scene);
		
	}




	public void Create_Login_Scene(Stage window)
	{
		GridPane login_grid=new GridPane();
		login_grid.setPadding(new Insets(10,10,10,10));
		login_grid.setVgap(10);
		login_grid.setHgap(10);
		
		//usename stuff
		Label uname_lbl=new Label("Trello Username:");
		GridPane.setConstraints(uname_lbl, 0, 0);
		
		TextField uname_txt=new TextField();
		uname_txt.setPromptText("username");
		GridPane.setConstraints(uname_txt, 1, 0);
		//pass stuff
		Label token=new Label("Trello Access Token:");
		GridPane.setConstraints(token, 0, 1);
		
		TextField token_txt=new TextField();
		GridPane.setConstraints(token_txt, 1, 1);
		
		Button login_button=new Button(" Log In to Trello!");
		login_button.setOnAction(e-> Validate_User(window,uname_txt.getText(),token_txt.getText()));
		GridPane.setConstraints(login_button, 1, 2);
		
		
		login_grid.getChildren().addAll(uname_lbl,uname_txt
				,token,token_txt,login_button);
		Login_scene=new Scene(login_grid,400,200);
	}
	public void Create_List_Select_Scene(Stage window)
	{	
		
		java.util.List<Board> boards=trello.getBoardsByMember(username);
		GridPane list_select_grid=new GridPane();
		list_select_grid.setPadding(new Insets(10,10,10,10));
		list_select_grid.setVgap(10);
		list_select_grid.setHgap(10);
		
		//Pick a list
		Label trellolist_lbl=new Label("Pick a List");
		GridPane.setConstraints(trellolist_lbl,0,0);
		
		ChoiceBox<String> trello_list=new ChoiceBox<String>();
		for(int i=0;i<boards.size();i++)
		   {
			trello_list.getItems().add(boards.get(i).getName());
		   }
		trello_list.getSelectionModel().select(0);
		
		
		GridPane.setConstraints(trello_list, 0,1);
		
		
		Button blog_button=new Button("To BackLog!");
		blog_button.setOnAction(e-> List_Selected(window,trello_list));
		GridPane.setConstraints(blog_button,0,2);
		
		list_select_grid.getChildren().addAll(trellolist_lbl,trello_list,blog_button);
		List_Select_Scene=new Scene(list_select_grid,300,200);
		
	}
	
	public void Create_BackLog_Scene(Stage window,java.util.List<Card> BackLogItems)
	{
		
		//Backlog Stuff
		BorderPane Backlog_Pane=new BorderPane();
		
		//Center Properties Edit
		GridPane BackLogItem_Grid=new GridPane();
		BackLogItem_Grid.setPadding(new Insets(10,10,10,10));
		BackLogItem_Grid.setVgap(10);
		BackLogItem_Grid.setHgap(10);
		Label properties_lbl=new Label("Properties");
		GridPane.setConstraints(properties_lbl, 0, 0);
		//Properties			
		Label backlogitem_namelbl=new Label("Name:");
		GridPane.setConstraints(backlogitem_namelbl, 0, 1);
		Label backlogitem_nametxt=new Label();
		GridPane.setConstraints(backlogitem_nametxt, 1, 1);
		Label backlogitem_storypts_lbl=new Label("Story Points:");
		GridPane.setConstraints(backlogitem_storypts_lbl, 0, 2);
		TextField backlogitem_storypts_val=new TextField();
		GridPane.setConstraints(backlogitem_storypts_val, 1, 2);
		Button backlogitem_addchild_button=new Button("Add Child");
		
		GridPane.setConstraints(backlogitem_addchild_button, 0, 3);
		TextField backlogitem_addchild_val=new TextField();
		GridPane.setConstraints(backlogitem_addchild_val, 1, 3);
		
		Label backlogitem_desc_lbl=new Label("Description:");
		GridPane.setConstraints(backlogitem_desc_lbl, 0, 4);
		TextArea backlogitem_desc_txt=new TextArea();
		backlogitem_desc_txt.setMaxWidth(500);
		backlogitem_desc_txt.setWrapText(true);
		GridPane.setConstraints(backlogitem_desc_txt, 1, 5);
		
		Label Soft_Cat_lbl=new Label("Component Software Category:");
		GridPane.setConstraints(Soft_Cat_lbl, 0, 6);
		ChoiceBox<String> software_cat=new ChoiceBox<String>();
		GridPane.setConstraints(software_cat, 1, 6);
		for (BackLogItem.Category cat: BackLogItem.Category.values())
	    {
			software_cat.getItems().add(cat.toString());
	    }
		software_cat.getSelectionModel().select(0);
		
		  
		
		
		Label Prob_lbl=new Label("Probability Component is:");
		GridPane.setConstraints(Prob_lbl, 0, 7);
		Label OS_Prob_lbl=new Label("Available In Open Source");
		GridPane.setConstraints(OS_Prob_lbl, 0, 8);
		TextField OS_Prob_txt=new TextField();
		OS_Prob_txt.setText("34");
		
		GridPane.setConstraints(OS_Prob_txt, 1, 8);
		Label FS_Prob_lbl=new Label("Coded From Scratch");
		GridPane.setConstraints(FS_Prob_lbl, 0, 9);
		TextField FS_Prob_txt=new TextField();
		FS_Prob_txt.setText("33");
		GridPane.setConstraints(FS_Prob_txt, 1, 9);
		Label LC_Prob_lbl=new Label("Available In Legacy Code");
		GridPane.setConstraints(LC_Prob_lbl, 0, 10);
		TextField LC_Prob_txt=new TextField();
		LC_Prob_txt.setText("33");
		GridPane.setConstraints(LC_Prob_txt, 1, 10);
		
		
		
		Label backlogitem_comm_lbl=new Label("Comments:");
		GridPane.setConstraints(backlogitem_comm_lbl, 0, 11);
		TextArea backlogitem_comm_txt=new TextArea();
		backlogitem_comm_txt.setMaxWidth(500);
		backlogitem_comm_txt.setWrapText(true);
		GridPane.setConstraints(backlogitem_comm_txt, 1, 12);
		
		Button updateButton=new Button("Update");
		GridPane.setConstraints(updateButton, 0, 13);
		
		
		Button edit_category_weights_button=new Button("Edit Software Category Weights");
		edit_category_weights_button.setOnAction(e->window.setScene(Software_Category_Weights_Scene));
		GridPane.setConstraints(edit_category_weights_button, 1, 13);
		
		Button toComponentGraph_button=new Button("Component Graph");
		GridPane.setConstraints(toComponentGraph_button, 0, 14);
		toComponentGraph_button.setOnAction(e->SwitchScene(window,Component_Graph_Scene));
		Button toSimulation_button=new Button("Simulation");
		toSimulation_button.setOnAction(e->SetSimulationScene(window));
		GridPane.setConstraints(toSimulation_button, 1, 14);
		Button pickanotherlist_button=new Button("Choose another project");
		pickanotherlist_button.setOnAction(e->window.setScene(List_Select_Scene));
		GridPane.setConstraints(pickanotherlist_button, 0, 15);
		
		
		
		
			
		
		backlogitem_addchild_button.setOnAction(e->AddChildToTree(BackLogEntryTree,backlogitem_nametxt.getText(),backlogitem_addchild_val.getText()));
		
		//Comments	

		VBox TreeBox=new VBox();
		TreeItem<String> troot=new TreeItem<String>(current_board.getName());
		ArrayList<String> ComponentList=new ArrayList<String>();
		
		for(int i=0;i<BackLogItems.size();i++)
		{
			Backlog.put(BackLogItems.get(i).getName(), new BackLogItem(BackLogItems.get(i)));
			ComponentList.add(BackLogItems.get(i).getName());	
		}
		
		
		BackLogEntryTree=BuildTree(troot,ComponentList);
		//BackLogEntryTree.getSelectionModel().selectedItemProperty().addListener((v,oldValue,newValue) -> SetSelectedItem(newValue,backlogitem_nametxt));
		
		BackLogEntryTree.getSelectionModel().selectedItemProperty().addListener((v,oldValue,newValue) ->
		UpdateBacklogItemFields(backlogitem_nametxt,backlogitem_desc_txt,backlogitem_storypts_val,backlogitem_comm_txt,OS_Prob_txt,FS_Prob_txt,LC_Prob_txt,software_cat,
				newValue));
		
		
		updateButton.setOnAction(e->UpdateBacklog(BackLogEntryTree.getSelectionModel().getSelectedItem(),
				backlogitem_nametxt,backlogitem_desc_txt,backlogitem_storypts_val,backlogitem_comm_txt,OS_Prob_txt,FS_Prob_txt,LC_Prob_txt,software_cat));
		toComponentGraph_button.setOnAction(e->window.setScene(Component_Graph_Scene));
		toSimulation_button.setOnAction(e->window.setScene(Simulation_Scene));
		edit_category_weights_button.setOnAction(e->window.setScene(Software_Category_Weights_Scene));
		//BackLogEntryTree.getSelectionModel().selectedItemProperty().addListener((v,oldValue,newValue) -> SetSelectedItem(newValue,backlogitem_nametxt));
		//BackLogEntryTree.getSelectionModel().selectedItemProperty().addListener((v,oldValue,newValue) -> SetSelectedItem(newValue,backlogitem_nametxt));
		TreeBox.getChildren().add(BackLogEntryTree);
		
		
		BackLogItem_Grid.getChildren().addAll(properties_lbl,
				backlogitem_namelbl,backlogitem_nametxt,
				backlogitem_storypts_lbl,backlogitem_storypts_val,
				backlogitem_addchild_button,backlogitem_addchild_val,
				backlogitem_desc_lbl,backlogitem_desc_txt,
				Soft_Cat_lbl,software_cat,
				Prob_lbl,
				OS_Prob_lbl,OS_Prob_txt,
				FS_Prob_lbl,FS_Prob_txt,
				LC_Prob_lbl,LC_Prob_txt,
				backlogitem_comm_lbl,backlogitem_comm_txt,
				updateButton,edit_category_weights_button,
				toComponentGraph_button,toSimulation_button,pickanotherlist_button);
		
		Backlog_Pane.setCenter(BackLogItem_Grid);
		TreeBox.setPrefHeight(800);
		TreeBox.setMinHeight(800);
		Backlog_Pane.setMinHeight(800);
		Backlog_Pane.setLeft(TreeBox);
		Create_Component_Graph(window);
		Backlog_Scene=new Scene(Backlog_Pane,1024,800);
		
		
		
	}
	
	private void SetSimulationScene(Stage window) {
		
		
		window.setScene(Simulation_Scene);
		
	}
	private void SetComponentGraphScene(Stage window) {
		Create_Component_Graph(window);
		window.setScene(Component_Graph_Scene);
		window.show();
	}
	private void UpdateBacklog(TreeItem<String> selectedItem,Label backlogitem_nametxt
			,TextArea backlogitem_desc_txt,TextField backlogitem_storypts_val,TextArea backlogitem_comm_txt
			,TextField OS_Prob_txt,TextField FS_Prob_txt,TextField LC_Prob_txt, ChoiceBox<String> software_cat) {
		BackLogItem backlogitem=Backlog.get(selectedItem.getValue());
		if(backlogitem!=null)
		{
			backlogitem.setName(backlogitem_nametxt.getText());
			backlogitem.setDescription(backlogitem_desc_txt.getText());
			backlogitem.setStoryPoints(Integer.parseInt(backlogitem_storypts_val.getText()));
			backlogitem.setComments(backlogitem_comm_txt.getText());
			backlogitem.setOS_Prob(Integer.parseInt(OS_Prob_txt.getText()));
			backlogitem.setFS_Prob(Integer.parseInt(FS_Prob_txt.getText()));
			backlogitem.setLC_Prob(Integer.parseInt(LC_Prob_txt.getText()));
			backlogitem.setLC_Prob(Integer.parseInt(LC_Prob_txt.getText()));
			backlogitem.setCat(BackLogItem.Category.valueOf(software_cat.getSelectionModel().getSelectedItem()));
		}
		
	}
	public void Create_Simulation_Scene(Stage window)
	{
		BorderPane simulation_pane=new BorderPane();
		TabPane simulation_pane_center=new TabPane();
		
		
		//INFO TAB 
		Tab info_tab=new Tab();
		info_tab.setText("Simulation_information");
		GridPane Simulation_info_Grid=new GridPane();
		Simulation_info_Grid.setPadding(new Insets(10,10,10,10));
		Simulation_info_Grid.setVgap(10);
		Simulation_info_Grid.setHgap(10);
		
		//Settings and  stuff
		Label num_runs_lbl=new Label("Number of runs:");
		GridPane.setConstraints(num_runs_lbl, 0, 0);
		
		TextField num_runs_txt=new TextField();
		num_runs_txt.setText("1000000");
		GridPane.setConstraints(num_runs_txt, 1, 0);
		//pass stuff
		Label sprint_duration_lbl=new Label("Sprint Duration(Days):");
		GridPane.setConstraints(sprint_duration_lbl, 0, 1);
		
		TextField sprint_duration_txt=new TextField();
		sprint_duration_txt.setText("10");
		GridPane.setConstraints(sprint_duration_txt, 1, 1);
		
		
		Label sprint_story_points_lbl=new Label("Sprint Story Points Completed(Team):");
		GridPane.setConstraints(sprint_story_points_lbl, 0, 2);
		
		TextField sprint_story_points_txt=new TextField();
		sprint_story_points_txt.setText("10");
		GridPane.setConstraints(sprint_story_points_txt, 1, 2);
		
		
		Button run_button=new Button("Run Simulation");
		run_button.setOnAction(e-> Run_Simulation(window,num_runs_txt,sprint_duration_txt,sprint_story_points_txt));
		GridPane.setConstraints(run_button, 1, 3);
		
		
		Simulation_info_Grid.getChildren().addAll(num_runs_lbl,num_runs_txt
				,sprint_duration_lbl,sprint_duration_txt,sprint_story_points_lbl,sprint_story_points_txt,run_button);
		
		info_tab.setContent(Simulation_info_Grid);
		info_tab.closableProperty().setValue(false);
		simulation_pane_center.getTabs().add(info_tab);
		
		//RESULTS TAB
		Tab results_tab=new Tab();
		results_tab.setText("Results");
		results_tab.closableProperty().setValue(false);
		GridPane Simulation_Results_Grid=new GridPane();
		Simulation_Results_Grid.setPadding(new Insets(10,10,10,10));
		Simulation_Results_Grid.setVgap(10);
		Simulation_Results_Grid.setHgap(10);
		
		//Results stuff
		Label story_points_lbl=new Label("Number of estimated story points:");
		GridPane.setConstraints(story_points_lbl, 0, 0);
		 num_story_points_lbl=new Label("Not Yet Calculated");
		GridPane.setConstraints(num_story_points_lbl, 1, 0);
		Label velocity_lbl=new Label("Estimated Average Velocity:");
		GridPane.setConstraints(velocity_lbl, 0, 1);
		 estimated_velocity_lbl=new Label("Not Yet Calculated");
		GridPane.setConstraints(estimated_velocity_lbl, 1, 1);
		Label sprints_lbl=new Label("Estimated Number of  Sprints:");
		GridPane.setConstraints(sprints_lbl, 0, 2);
		 estimated_sprints_lbl=new Label("Not Yet Calculated");
		GridPane.setConstraints(estimated_sprints_lbl, 1, 2);
		Label num_days_lbl=new Label("Estimated Days to complete project:");
		GridPane.setConstraints(num_days_lbl, 0, 3);
		 estimated_num_days_lbl=new Label("Not Yet Calculated");
		GridPane.setConstraints(estimated_num_days_lbl, 1, 3);
		
		Button toBacklog=new Button(" Back to Backlog");
		GridPane.setConstraints(toBacklog, 0, 4);
		toBacklog.setOnAction(e->SetBackLogScene(window) );
		
		Button toComponentTree=new Button(" To Component Tree");
		GridPane.setConstraints(toComponentTree, 1, 4);
		toComponentTree.setOnAction(e->window.setScene(Component_Graph_Scene) );
		
		
		
		Simulation_Results_Grid.getChildren().addAll(story_points_lbl,num_story_points_lbl,velocity_lbl,estimated_velocity_lbl,
				sprints_lbl,estimated_sprints_lbl,num_days_lbl,estimated_num_days_lbl,toBacklog,toComponentTree);
		
		results_tab.setContent(Simulation_Results_Grid);
		simulation_pane_center.getTabs().add(results_tab);
		
		
		
		//BURNDOWN CHART
		Tab burndown_chart=new Tab();
		burndown_chart.setText("burndown_chart");
		burndown_chart.closableProperty().setValue(false);
		final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        BurndownChart= 
            new AreaChart<>(xAxis,yAxis);
        BurndownChart.setTitle("BackLog");
 
        burndown_chart.setContent(BurndownChart);
		simulation_pane_center.getTabs().add(burndown_chart);
		simulation_pane.setCenter(simulation_pane_center);
		Simulation_Scene=new Scene(simulation_pane,900,400);
		window.show();
	}
	
	
	private void Run_Simulation(Stage window, TextField num_runs_txt, TextField sprint_duration_txt, TextField sprint_story_points_txt) {
		ArrayList<BackLogItem> components=new ArrayList<BackLogItem>();
		isLeaf(components,BackLogEntryTree.getRoot());
		Simulation sim=new Simulation(components, Integer.parseInt(num_runs_txt.getText()), 
				Integer.parseInt(sprint_duration_txt.getText()), Integer.parseInt(sprint_story_points_txt.getText()),multipliers);
		
		sim.Run();
		
		
		double total_average_story_points=0,total_fs_story_points=0;
        double total_days=0;
		//Setup Results
		ArrayList<Component> sim_components=sim.getComponents();
        for(int i=0;i<sim_components.size();i++)
        {
        	//Calculate Statistics
        	total_average_story_points+=sim_components.get(i).getAvgStoryPoints();
        	total_fs_story_points+=sim_components.get(i).getStory_Points();
        	total_days+=sim_components.get(i).getAvgStoryPoints()/Integer.parseInt(sprint_story_points_txt.getText())*Integer.parseInt(sprint_duration_txt.getText());
        	System.out.println(sim_components.get(i).getAvgStoryPoints());
        	//Color Tree
        	ColorComponent(sim_components.get(i));
        	
        }
		
		//Set Up Results
		estimated_velocity_lbl.setText(String.valueOf(sim.Velocity()));
		num_story_points_lbl.setText(String.valueOf(sim.Estimated_Story_Points()));
		estimated_num_days_lbl.setText(String.valueOf(total_days));
		estimated_sprints_lbl.setText(String.valueOf(sim.Estimated_Sprints()));
		
		
		//Set Up Burndown Chart
        XYChart.Series BacklogPoints= new XYChart.Series();
        XYChart.Series FromScratch= new XYChart.Series();
        BacklogPoints.setName("Estimated Project Burndown Tree");
        FromScratch.setName("Project Burndown Tree Coding From Scratch");
        
        double current_story_points=total_average_story_points;
        double current_fs_story_points=total_fs_story_points;
        double running_days=0;
        BacklogPoints.getData().add(new XYChart.Data (0,current_story_points));
        FromScratch.getData().add(new XYChart.Data (0,current_fs_story_points));
        for(int i=0;i<sim_components.size();i++)
        {	
        	current_fs_story_points-=sim_components.get(i).getStory_Points();
        	current_story_points-=sim_components.get(i).getAvgStoryPoints();
        	running_days+=sim_components.get(i).getAvgStoryPoints()/Integer.parseInt(sprint_story_points_txt.getText())*Integer.parseInt(sprint_duration_txt.getText());
        	BacklogPoints.getData().add(new XYChart.Data (i+1,current_story_points));
        	FromScratch.getData().add(new XYChart.Data (i+1,current_fs_story_points));
        	System.out.println("Average Story Points For Component:"+sim_components.get(i).getName()+":"+sim_components.get(i).getAvgStoryPoints());
        		System.out.println(current_story_points);
        }
        BurndownChart.getData().clear();
        BurndownChart.getData().add(BacklogPoints);
        BurndownChart.getData().add(FromScratch);
        
         
        
		
	}
	private void ColorComponent(Component component) {
		LabelCell lcell=(LabelCell) graph.getModel().GetCellMap().get(component.getName());
    	
    	switch (component.Dominant_Choice())
    	{
    	case OS:
    		lcell.SetStyle("-fx-border-color:black; -fx-background-color: dodgerblue;");
    		break;
    	case FS:
    		lcell.SetStyle("-fx-border-color:black; -fx-background-color: red;");
    		break;
    	case LC:
    		lcell.SetStyle("-fx-border-color:black; -fx-background-color: green;");
    		break;
    		
    	}
		
	}
	private void  isLeaf(ArrayList<BackLogItem> components,TreeItem<String> item) {
		
		
        if(item.isLeaf())
        {
        	components.add(Backlog.get(item.getValue()));
        }
        else
        {
        	for(int i=0;i<item.getChildren().size();i++)
        	isLeaf(components,item.getChildren().get(i));
        }
        	
        
		
	}
	
	public void Initialize(Stage window)
	{
		Backlog=new HashMap<String,BackLogItem>();
		 	  
			//Login Scene 
		   Create_Login_Scene(window);
		   Create_Simulation_Scene(window);
		   multipliers=new HashMap<BackLogItem.Category,CostMultiplierPair>();
		   Create_Software_Category_Weights_Scene(window);
	}
	
	
	private void Create_Component_Graph(Stage window) {
		// TODO Auto-generated method stub
        BorderPane root = new BorderPane();
        graph = new Graph();
        root.setCenter(graph.getScrollPane());
        Component_Graph_Scene = new Scene(root, 1920, 1024);
        addGraphComponents();
        ButtonCell backlogCell=((ButtonCell)(graph.getModel().GetCellMap().get("ToBackLog")));
        backlogCell.getButton().setOnAction(e->window.setScene(Backlog_Scene));
        backlogCell.setLayoutX(0);
        backlogCell.setLayoutY(0);
        ButtonCell simulationCell=((ButtonCell)(graph.getModel().GetCellMap().get("ToSimulation")));
        simulationCell.getButton().setOnAction(e->window.setScene(Simulation_Scene));
        simulationCell.setLayoutX(0);
        simulationCell.setLayoutY(30);
        Layout layout = new TreeLayout(graph,BackLogEntryTree);
        layout.execute();
		
	}
	
	
	private void SetBackLogScene(Stage window) {
		window.setScene(Backlog_Scene);
		window.show();
		
	}
	private void addGraphComponents() {

        Model model = graph.getModel();
        
        TreeItem<String> root=BackLogEntryTree.getRoot();
        graph.beginUpdate();
        model.addCell(root.getValue(), CellType.LABEL);
        AddChildren(root);
        AddButton("ToBackLog");
        AddButton("ToSimulation");
        
        graph.endUpdate();

    }
	private void AddButton(String string) {
		// 
		Model model = graph.getModel();
		model.addCell(string, CellType.BUTTON);
		
	}
	private void AddChildren(TreeItem<String> root) {
		Model model = graph.getModel();
		for(int i=0;i<root.getChildren().size();i++)
		{
			model.addCell(root.getChildren().get(i).getValue(), CellType.LABEL);
			model.addEdge(root.getValue(), root.getChildren().get(i).getValue());
			AddChildren(root.getChildren().get(i));
		}
		
	}
	public void start(Stage window) {
		
		
	
			Initialize(window);
			
			
		
			
			window.setScene(Login_scene);
			window.setTitle("Agile-Component-Sim");
			window.show();
			
		
	}
	
	private void Validate_User(Stage window,String username,String token)
	{	
		if(!username.equals(""))
		{this.username=username;
		 token="4e8ce796ddf8391ec58f9a0bd5e305faabae003181ba0037a0d89c65c2f8addd";
		trello= new TrelloImpl("df7a1ef81b3ba83a91c2e3420fb4bfd9", token);
		Create_List_Select_Scene(window);
		window.setScene(List_Select_Scene);
		}
		else
		{	Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("No Username Error");
			alert.setContentText("Username Cannot be emtpy");

			alert.showAndWait();
		}
		//Pass stuff to the List_Select;
		
		
	}
	
	private void List_Selected(Stage window,ChoiceBox<String> choiceBox)
	{
		
		//Create_BackLog_Scene(window,String BoardId);
		
		current_board=trello.getBoardsByMember(username).get(choiceBox.getSelectionModel().getSelectedIndex());
		
		java.util.List<org.trello4j.model.List> list=trello.getListByBoard(current_board.getId());
		int idx=-1;
		for(int i=0;i<list.size();i++)
		{
			if(list.get(i).getName().equals("BackLog"))
			{
				idx=i;
			}
		}
		
		if(idx!=-1)
		{
			java.util.List<Card> cards=trello.getCardsByList(list.get(idx).getId());
		
		Create_BackLog_Scene(window,cards);
		window.setScene(Backlog_Scene);
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("No Backlog Found");
			alert.setContentText("Selected Board Does not seem to contain a Backlog list");
			alert.showAndWait();
			
		}
		
	}
	
	private void SwitchScene(Stage window,Scene scene)
	{
		window.setScene(scene);
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	private void MakeBranch(String name,TreeItem<String> parent)
	{
		TreeItem<String> node=new TreeItem<String>(name);
		node.setExpanded(true);
		parent.getChildren().add(node);
	
	}
	
	private  TreeView<String> BuildTree(TreeItem<String> root,ArrayList<String> Components)
	{
		TreeView<String> tree=new TreeView(root);
		root.expandedProperty().setValue(true);
		for(int i=0;i<Components.size();i++)
		{
			MakeBranch(Components.get(i),root);
		}
	
		return tree;
	}
	
	private void ShowBacklogitem(BackLogItem item)
	{
		
		
	}
	private void UpdateBacklogItemFields(Label name_lbl,TextArea backlogitem_desc_txt,TextField Story_Points_tf,TextArea backlogitem_comm_txt,
			TextField oS_Prob_txt, TextField fS_Prob_txt, TextField lC_Prob_txt, ChoiceBox<String> software_cat, TreeItem<String> newValue)
	{
		if(Backlog.get(newValue.getValue())!=null)
		{name_lbl.setText(Backlog.get(newValue.getValue()).getName());
		backlogitem_desc_txt.setText(Backlog.get(newValue.getValue()).getDescription());
		Story_Points_tf.setText(String.valueOf(Backlog.get(newValue.getValue()).getStoryPoints()));
		backlogitem_comm_txt.setText(Backlog.get(newValue.getValue()).getComments());
		if(Backlog.get(newValue.getValue()).getCat()!=null)
		software_cat.getSelectionModel().select(Backlog.get(newValue.getValue()).getCat().toString());
		oS_Prob_txt.setText(String.valueOf(Backlog.get(newValue.getValue()).getOS_Prob()));
		fS_Prob_txt.setText(String.valueOf(Backlog.get(newValue.getValue()).getFS_Prob()));
		lC_Prob_txt.setText(String.valueOf(Backlog.get(newValue.getValue()).getLC_Prob()));
		}
		
	}
	
	private void SetSelectedItem(TreeItem<String> selected_item,Label field)
	{
		
		
		field.setText(selected_item.getValue());
		
	
	}
	
	
	private TreeItem<String> FindItem(TreeItem<String> root,String Value){
        System.out.println("Current Parent :" + root.getValue());
        for(TreeItem<String> child: root.getChildren())
        {
            if(child.getChildren().isEmpty())
            {
                if(child.getValue()==Value)
                	return child;
                
            } 
            else 
            {
            	return FindItem(child,Value);
            }
        }
		return null;
    }

	
	
	private void AddChildToTree(TreeView<String> tree,String node,String newNode)
	{
		if(!node.isEmpty() && !newNode.isEmpty())
		{	
			
			Backlog.put(newNode, new BackLogItem(newNode,"","",-1,-1,-1,-1));
			TreeIterator<String> treeit=new TreeIterator(tree.getRoot());
			while(treeit.hasNext()!=false)
			{
				TreeItem<String> treenode=treeit.next();
				if(treenode.getValue()==node)
				{
					MakeBranch(newNode,treenode);
				}
			}
			
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("AddChild Error");
			alert.setContentText("Child Name Should Not Be Blank and an allready existing component must be selected");

			alert.showAndWait();
		}
			
		
	}
	
	
}
