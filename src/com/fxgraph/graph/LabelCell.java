package com.fxgraph.graph;

import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;

import com.fxgraph.graph.Cell;

public class LabelCell extends Cell {

    public LabelCell(String id) {
        super(id);
        this.setWidth(100);
        this.setStyle("-fx-border-color:black; -fx-background-color: gray;");
        Label view = new Label(id);
        view.setMinWidth(100);
        view.setMaxWidth(200);
        setView(view);

    }
    
    public void SetStyle(String Style)
    {
    	this.setStyle(Style);
    }

}