package com.fxgraph.graph;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

import com.fxgraph.graph.Cell;
import com.fxgraph.graph.Graph;

import application.TreeIterator;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;


public class TreeLayout extends Layout {

    Graph graph;
    TreeView tree;
    int screen_width=1920;

    int screen_height=1080;

    Random rnd = new Random();
    HashMap<String,Integer> weights=new HashMap();

    public TreeLayout(Graph graph,TreeView<String> ComponentTree) {

        this.graph = graph;
        this.tree=ComponentTree;
    }

    public void execute() {
    		
    		
    		
    		DFS_Weights(tree);//Compute Weights
    		Map<String,Cell> CellMap=graph.getModel().GetCellMap();//Map Containing all cells
    		//Place root at the top center --> screen_width-cell_width, 0
    		Cell current=CellMap.get(tree.getRoot().getValue());
    		current.relocate((screen_width-current.getWidth())/2, 0);
    		System.out.println("Root x,y:"+current.getLayoutX()+","+current.getLayoutY());
    		//Recursivelly place children at right offsets;
    		RelocateChildren(tree.getRoot(),current,1);
    		
    		
    		
    		
    		
    		
    		
    		
    		
    	
      
    }
    
    private void RelocateChildren(TreeItem<String> treeitem, Cell current,int current_height) {
    	System.out.println("Placing "+treeitem.getValue()+"'s children");
    	if(!treeitem.isLeaf())
    	{//Get Weight and split it to the children
    	Map<String,Cell> CellMap=graph.getModel().GetCellMap();//Map Containing all cells
    	double weight=weights.get(treeitem.getValue());
    	System.out.println("Weight:"+weight+" Num_Children:"+treeitem.getChildren().size()+"step:"+weight/treeitem.getChildren().size());
    	
    	for(int i=0;i<treeitem.getChildren().size();i++)
    	{	
    		
    		
    		Cell the_cell=CellMap.get(treeitem.getChildren().get(i).getValue());
    		if(the_cell!=null)
    		{
    		System.out.println("Width"+the_cell.getWidth());
    		double new_x=current.getLayoutX()-(weight/treeitem.getChildren().size())*the_cell.getWidth()+i*2*weight/treeitem.getChildren().size()*the_cell.getWidth();
    		double new_y=50*current_height;
    		System.out.println("Child:"+treeitem.getChildren().get(i).getValue()+" new_x,new_y"+new_x+","+new_y);
    		the_cell.relocate(new_x,new_y);
    		RelocateChildren(treeitem.getChildren().get(i),the_cell,current_height+1);
    		}
    	}
    	}
    	
		
	}

	public void DFS_Weights(TreeView<String> tree)
    {
    	Stack<TreeItem<String>> stack=new Stack<TreeItem<String>>();
    	HashSet<String>  visited= new HashSet();
    	stack.push(tree.getRoot());
    	TreeItem<String> current=null;
    	while(!stack.isEmpty())
    	{
    		
    		current=stack.peek();
    		System.out.println("Current: "+current.getValue());
    		visited.add(current.getValue());
    		
    		for(int i=0;i<current.getChildren().size();i++)
    		{
    			if(!visited.contains(current.getChildren().get(i).getValue()))
    			stack.push(current.getChildren().get(i));
    		}
    		
    		if(AllChildrenVisited(current,visited))
    		{
    			if(current!=tree.getRoot())
    			{
    			if(weights.containsKey(current.getParent().getValue()))
    			{
    				if(weights.containsKey(current.getValue()))
    				weights.put(current.getParent().getValue(), weights.get(current.getParent().getValue())+weights.get(current.getValue()));
    				else
    				{
    					weights.put(current.getParent().getValue(), weights.get(current.getParent().getValue())+1);
    				}
    				
    				
    			}
    			else
    			{if(weights.containsKey(current.getValue()))
    				weights.put(current.getParent().getValue(), weights.get(current.getValue()));
    			else
    				weights.put(current.getParent().getValue(), 1);
    			}
    			}
    			System.out.println("Popping: "+current.getValue());
    			stack.pop();
    		}
    		
    	}
    	
    	System.out.println("Resulting Weights:");
    	Iterator it = weights.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            //it.remove(); // avoids a ConcurrentModificationException
        }
    	
     	
    	
    }

	private boolean AllChildrenVisited(TreeItem<String> current,HashSet<String> visited) {
		
		
		for(int i=0;i<current.getChildren().size();i++)
		{
			if(!visited.contains(current.getChildren().get(i).getValue()))
			{
				return false;
			}
		}

		return true;
	}
    
    
    
    
    
}

