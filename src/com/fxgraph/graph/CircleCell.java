package com.fxgraph.graph;




import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import com.fxgraph.graph.Cell;

public class CircleCell extends Cell {

    public CircleCell( String id) {
        super( id);

        Circle view = new Circle(25);
        

        view.setStroke(Color.DODGERBLUE);
        view.setFill(Color.DODGERBLUE);

        setView( view);

    }

}