\contentsline {chapter}{\numberline {1}Agile-Scrum}{13}
\contentsline {section}{\numberline {1.1}Scrum}{16}
\contentsline {subsection}{\numberline {1.1.1}Roles}{16}
\contentsline {subsection}{\numberline {1.1.2}Workflow}{18}
\contentsline {subsection}{\numberline {1.1.3}Artifacts}{21}
\contentsline {subsection}{\numberline {1.1.4}Definitions}{23}
\contentsline {chapter}{\numberline {2}Simulation}{25}
\contentsline {section}{\numberline {2.1}Monte Carlo Simulation}{26}
\contentsline {subsection}{\numberline {2.1.1}function}{27}
\contentsline {chapter}{\numberline {3}Software Cost Estimation}{29}
\contentsline {section}{\numberline {3.1}Traditional Software Cost Estimation}{29}
\contentsline {chapter}{\numberline {4}Cost Estimation Techniques in Agile Projects}{31}
\contentsline {section}{\numberline {4.1}Cost Estimation Methods}{31}
\contentsline {section}{\numberline {4.2}Failure of Traditional Methods}{31}
\contentsline {section}{\numberline {4.3}Methods Focused on Agile}{31}
\contentsline {chapter}{\numberline {5}Our Method}{33}
\contentsline {section}{\numberline {5.1}Backlog to Components}{33}
\contentsline {section}{\numberline {5.2}Simulation Attributes}{34}
\contentsline {subsection}{\numberline {5.2.1}Post Multiply Normalization}{35}
\contentsline {subsection}{\numberline {5.2.2}Block Exponent}{35}
\contentsline {section}{\numberline {5.3}Integer optimizations}{36}
\contentsline {subsection}{\numberline {5.3.1}Conversion to fixed point}{36}
\contentsline {subsection}{\numberline {5.3.2}Small Constant Multiplications}{37}
\contentsline {section}{\numberline {5.4}Other optimizations}{38}
\contentsline {subsection}{\numberline {5.4.1}Low-level parallelism}{38}
\contentsline {subsection}{\numberline {5.4.2}Pipeline optimizations}{39}
\contentsline {chapter}{\numberline {A}Tables}{41}
\contentsline {chapter}{\numberline {B}Figures}{43}
